# node-kit End-Point Generator

The node-kit end-point generator for the @kwaeri/node-kit platform. It's included through the @kwaeri/generators entry-point.